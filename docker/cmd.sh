#!/bin/sh
echo 'Starting containers...'
#docker-compose up -d
#sleep 10
docker-compose run -d db
docker-compose run -d chrome
docker-compose run -d web

docker-compose run composer
docker-compose run codeception run
echo 'Stop containers...'
docker-compose down